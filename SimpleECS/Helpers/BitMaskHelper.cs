﻿using System;
using Ecs.Contracts.Entities;

namespace Ecs.Helpers
{
    public static class BitMaskHelper
    {
        public static ulong ToBitMask(IComponentType componentType)
        {
            return Convert.ToUInt64(1 << componentType.Index);
        }

        public static ulong ManyToBitMask(params IComponentType[] componentTypes)
        {
            if (componentTypes == null || componentTypes.Length == 0)
            {
                throw new InvalidOperationException("Array 'componentTypes' couldn't be null or empty."); 
            }

            ulong mask = 0;
            for (int i = 0; i < componentTypes.Length; i++)
            {
                BitMaskHelper.AddFlagToMask(ref mask, ToBitMask(componentTypes[i]));
            }

            return mask;
        }

        public static void AddFlagToMask(ref ulong mask, ulong flag)
        {
            mask |= flag;
        }

        public static void RemoveFlagFromMask(ref ulong mask, ulong flag)
        {
            mask &= ~flag;
        }

        public static bool HasFlagInMask(ulong mask, ulong flag)
        {
            return (mask & flag) == flag;
        }
    }
}