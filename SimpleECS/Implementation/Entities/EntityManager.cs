﻿using System;
using System.Collections.Generic;
using Ecs.Contracts;
using Ecs.Contracts.Entities;
using Ecs.Contracts.Pool;
using Ecs.Helpers;
using Ecs.Implementation.Pool;
using Ecs.Types;

namespace Ecs.Implementation.Entities
{
    public class EntityManager : IEntityManager
    {
        private readonly IList<KeyValuePair<ulong, IList<IEntity>>> _groups;
        private readonly IPool<IEntity> _pool;

        public EntityManager(int initialEntityPoolSize, int componentSize)
        {
            if (initialEntityPoolSize < 0)
            {
                throw new ArgumentException("Pool size can't be less 0");
            }

            if (componentSize < 0)
            {
                throw new ArgumentException("Component size can't be less 0");
            }

            _groups = new List<KeyValuePair<ulong, IList<IEntity>>>();
            _pool = new EntityPool(initialEntityPoolSize, new EntityPoolObjectProvider(componentSize));
        }

        public IEntity CreateEntity()
        {
            var entity = _pool.Get();
            entity.ComponentChanged += EntityOnComponentChanged;

            return entity;
        }

        public void RemoveEntity(IEntity entity)
        {
            _pool.Release(entity);
            entity.ComponentChanged -= EntityOnComponentChanged;
        }

        public IList<IEntity> GetAllEntities()
        {
            return _pool.GetInUsedEntities();
        }

        public IList<IEntity> GetEntityGroup(IMatcher matcher)
        {
            if (matcher == null)
            {
                throw new ArgumentNullException("Parameter 'matcher' can't be null.");
            }

            IList<IEntity> entities = null;
            for (var i = 0; i < _groups.Count; i++)
            {
                if (_groups[i].Key == matcher.Mask)
                {
                    entities = _groups[i].Value;
                    break;
                }
            }

            if (entities == null)
            {
                entities = CollectEntitiesByMask(matcher);
                _groups.Add(new KeyValuePair<ulong, IList<IEntity>>(matcher.Mask, entities));
            }

            return entities;
        }

        private IList<IEntity> CollectEntitiesByMask(IMatcher matcher)
        {
            var entities = GetAllEntities();
            var matchedEntities = new List<IEntity>();
            for (int i = 0; i < entities.Count; i++)
            {
                if (matcher.Match(entities[i].ComponentsMask))
                {
                    matchedEntities.Add(entities[i]);
                }
            }

            return matchedEntities;
        }

        private void EntityOnComponentChanged(IEntity entity, IComponentType componentType, OperationType operationType)
        {
            if (operationType == OperationType.Add)
            {
                AddEntityToGroup(entity, componentType);
            }
            else
            {
                RemoveEntityFromGroup(entity, componentType);
            }
        }

        private void RemoveEntityFromGroup(IEntity entity, IComponentType componentType)
        {
            for (int i = 0; i < _groups.Count; i++)
            {
                var entityGroup = _groups[i];
                if (BitMaskHelper.HasFlagInMask(entity.ComponentsMask | BitMaskHelper.ToBitMask(componentType), entityGroup.Key)
                    && BitMaskHelper.HasFlagInMask(entityGroup.Key, BitMaskHelper.ToBitMask(componentType)))
                {
                    entityGroup.Value.Remove(entity);
                }
            }
        }

        private void AddEntityToGroup(IEntity entity, IComponentType componentType)
        {
            for (int i = 0; i < _groups.Count; i++)
            {
                var entityGroup = _groups[i];
                if (BitMaskHelper.HasFlagInMask(entity.ComponentsMask, entityGroup.Key)
                    && BitMaskHelper.HasFlagInMask(entityGroup.Key, BitMaskHelper.ToBitMask(componentType)))
                {
                    entityGroup.Value.Add(entity);
                }
            }
        }
    }
}