﻿using System;
using Ecs.Contracts.Entities;
using Ecs.Helpers;
using Ecs.Types;

namespace Ecs.Implementation.Entities
{
    public class Entity : IEntity
    {
        public event ComponentChangedDelegate ComponentChanged;

        public ulong ComponentsMask
        {
            get { return _componentsMask; }
        }

        private readonly IComponent[] _components;
        private ulong _componentsMask;

        public Entity(int componentsSize)
        {
            if (componentsSize < 0)
            {
                throw new ArgumentException("Components size can't be less 0");
            }

            ComponentChanged = delegate {};
            _components = new IComponent[componentsSize];
        }

        public bool HasComponent(IComponentType componentType)
        {
            return BitMaskHelper.HasFlagInMask(_componentsMask, BitMaskHelper.ToBitMask(componentType));
        }

        public IComponent GetComponent(IComponentType componentType)
        {
            return _components[componentType.Index];
        }

        public void AddComponent(IComponent component)
        {
            if (component == null)
            {
                throw new ArgumentNullException("Parameter 'component' can't be null.");
            }

            if (!HasComponent(component.Type))
            {
                BitMaskHelper.AddFlagToMask(ref _componentsMask, BitMaskHelper.ToBitMask(component.Type));
                _components[component.Type.Index] = component;
                ComponentChanged(this, component.Type, OperationType.Add);
            }
            else
            {
                throw new InvalidOperationException(string.Format("Can't add '{0}' because it's already added.",
                    component.Type));
            }
        }

        public void RemoveComponent(IComponentType componentType)
        {
            if (HasComponent(componentType))
            {
                BitMaskHelper.RemoveFlagFromMask(ref _componentsMask, BitMaskHelper.ToBitMask(componentType));
                _components[componentType.Index] = null;
                ComponentChanged(this, componentType, OperationType.Remove);
            }
            else
            {
                throw new InvalidOperationException(string.Format("Can't remove '{0}' because it's already removed.",
                    componentType));
            }
        }

        public void ResetEntity()
        {
            for (var i = 0; i < _components.Length; i++)
            {
                if (_components[i] != null)
                {
                    ComponentChanged(this, _components[i].Type, OperationType.Remove);
                }
            }

            Array.Clear(_components, 0, _components.Length);
            _componentsMask = ulong.MinValue;
        }
    }
}