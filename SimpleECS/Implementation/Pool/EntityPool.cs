﻿using Ecs.Contracts.Entities;
using Ecs.Contracts.Pool;

namespace Ecs.Implementation.Pool
{
    public class EntityPool : AbstractPool<IEntity>
    {
        public EntityPool(int initialPoolSize, IPoolObjectProvider<IEntity> poolObjectProvider)
            : base(initialPoolSize, poolObjectProvider)
        {
        }

        protected override void CleanUp(IEntity @object)
        {
            @object.ResetEntity();
        }
    }
}