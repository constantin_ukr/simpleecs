﻿using System;
using Ecs.Contracts.Entities;
using Ecs.Contracts.Pool;
using Ecs.Implementation.Entities;

namespace Ecs.Implementation.Pool
{
    public class EntityPoolObjectProvider : IPoolObjectProvider<IEntity>
    {
        private readonly int _componentSize;

        public EntityPoolObjectProvider(int componentSize)
        {
            if (componentSize < 0)
            {
                throw new ArgumentException("Component size can't be less 0");
            }

            _componentSize = componentSize;
        }

        public IEntity Create()
        {
            return new Entity(_componentSize);
        }
    }
}