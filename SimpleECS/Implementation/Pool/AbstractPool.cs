﻿using System;
using System.Collections.Generic;
using Ecs.Contracts.Pool;

namespace Ecs.Implementation.Pool
{
    public abstract class AbstractPool<T> : IPool<T>
    {
        private readonly IPoolObjectProvider<T> _poolObjectProvider;

        private readonly IList<T> _available;
        private readonly IList<T> _inUse;

        private readonly object _locker;

        protected AbstractPool(int initialPoolSize, IPoolObjectProvider<T> poolObjectProvider)
        {
            _poolObjectProvider = poolObjectProvider;
            if (initialPoolSize < 0)
            {
                throw new ArgumentException("Pool size can't be less 0");
            }

            _available = new List<T>(initialPoolSize);
            _inUse = new List<T>(initialPoolSize);

            _locker = new object();

            Populate(initialPoolSize);
        }

        public T Get()
        {
            lock (_locker)
            {
                if (_available.Count != 0)
                {
                    var po = _available[0];
                    _inUse.Add(po);
                    _available.RemoveAt(0);
                    return po;
                }
                else
                {
                    var po = _poolObjectProvider.Create();
                    _inUse.Add(po);
                    return po;
                }
            }
        }

        public void Release(T @object)
        {
            if (@object == null)
            {
                throw new ArgumentNullException("Parameter 'object' can't be null.");
            }

            if (!_inUse.Contains(@object))
            {
                throw new InvalidOperationException(string.Format("Can't release '{0}' becaus it doesn't exist in 'inUse' collection.", @object));
            }

            lock (_locker)
            {
                CleanUp(@object);

                _available.Add(@object);
                _inUse.Remove(@object);
            }
        }

        public IList<T> GetInUsedEntities()
        {
            return _inUse;
        }

        protected abstract void CleanUp(T @object);

        private void Populate(int initialPoolSize)
        {
            for (var i = 0; i < initialPoolSize; i++)
            {
                _available.Add(_poolObjectProvider.Create());
            }
        }
    }
}