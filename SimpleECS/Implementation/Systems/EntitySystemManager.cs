﻿using Ecs.Contracts;
using Ecs.Contracts.Entities;

namespace Ecs.Implementation.Systems
{
    public class EntitySystemManager : SystemManager<IEntity>
    {
        public EntitySystemManager(IWorld world) : base(world)
        {
        }
    }
}