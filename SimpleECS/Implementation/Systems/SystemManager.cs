﻿using System;
using System.Collections.Generic;
using Ecs.Contracts;
using Ecs.Contracts.Systems;

namespace Ecs.Implementation.Systems
{
    public abstract class SystemManager<T> : ISystemManager
    {
        private readonly IWorld _world;

        private readonly IList<IInitializeSystem> _initializeSystems;
        private readonly IList<IUpdateSystem> _updateSystems;
        private readonly IList<IFixedUpdateSystem> _fixedUpdateSystems;
        private readonly IList<ISystem> _allSystems;

        protected SystemManager(IWorld world)
        {
            if (world == null)
            {
                throw new ArgumentNullException("Parameter 'world' can't be null.");
            }

            _world = world;
            _initializeSystems = new List<IInitializeSystem>();
            _updateSystems = new List<IUpdateSystem>();
            _fixedUpdateSystems = new List<IFixedUpdateSystem>();
            _allSystems = new List<ISystem>();
        }

        public void Initialize()
        {
            for (var i = 0; i < _allSystems.Count; i++)
            {
                _allSystems[i].World = _world;
            }

            for (var i = 0; i < _initializeSystems.Count; i++)
            {
                _initializeSystems[i].Initialize();
            }
        }

        public void Update()
        {
            for (var i = 0; i < _updateSystems.Count; i++)
            {
                _updateSystems[i].Update();
            }
        }

        public void FixedUpdate()
        {
            for (var i = 0; i < _fixedUpdateSystems.Count; i++)
            {
                _fixedUpdateSystems[i].FixedUpdate();
            }
        }

        public void Add(ISystem system)
        {
            if (system == null)
            {
                throw new ArgumentNullException("Parameter 'system' can't be null.");
            }

            AddSystem(system, _allSystems);
            AddSystem(system, _initializeSystems);
            AddSystem(system, _updateSystems);
            AddSystem(system, _fixedUpdateSystems);
        }

        public void Remove(ISystem system)
        {
            if (system == null)
            {
                throw new ArgumentNullException("Parameter 'system' can't be null.");
            }

            RemoveSystem(system, _initializeSystems);
            RemoveSystem(system, _updateSystems);
            RemoveSystem(system, _fixedUpdateSystems);
        }

        private void AddSystem<TSystem>(ISystem system, IList<TSystem> list) where TSystem : class, ISystem
        {
            var concreteSystem = system as TSystem;
            if (concreteSystem != null)
            {
                if (list.Contains(concreteSystem))
                {
                    throw new InvalidOperationException(string.Format("System '{0}' is already existed in list.", concreteSystem));
                }

                list.Add(concreteSystem);
            }
        }

        private void RemoveSystem<TSystem>(ISystem system, IList<TSystem> list) where TSystem : class, ISystem
        {
            var concreteSystem = system as TSystem;
            if (concreteSystem != null)
            {
                if (!list.Contains(concreteSystem))
                {
                    throw new InvalidOperationException(string.Format("System '{0}' is not existed in list", concreteSystem));
                }

                list.Remove(concreteSystem);
            }
        }
    }
}