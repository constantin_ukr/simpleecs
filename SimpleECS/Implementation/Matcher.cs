﻿using System;
using System.Collections.Generic;
using Ecs.Contracts;
using Ecs.Contracts.Entities;
using Ecs.Helpers;

namespace Ecs.Implementation
{
    public sealed class Matcher : IMatcher
    {
        #region Static Matcher
        private readonly static IDictionary<ulong, IMatcher> Matchers;

        static Matcher()
        {
            Matchers = new Dictionary<ulong, IMatcher>();
        }

        public static IMatcher GetMatcher(params IComponentType[] componentTypes)
        {
            if (componentTypes == null)
            {
                throw new ArgumentNullException("Parameter 'componentTypes' can't be null");
            }

            if (componentTypes.Length == 0)
            {
                throw new ArgumentException("Parameter 'componentTypes' can't be empty");
            }

            var matcherMask = GetMatcherMask(componentTypes);
            if (!Matchers.ContainsKey(matcherMask))
            {
                Matchers.Add(matcherMask, new Matcher(matcherMask));
            }

            return Matchers[matcherMask];
        }

        private static ulong GetMatcherMask(IComponentType[] componentTypes)
        {
            ulong matcherMask = 0;
            for (int i = 0; i < componentTypes.Length; i++)
            {
                BitMaskHelper.AddFlagToMask(ref matcherMask, BitMaskHelper.ToBitMask(componentTypes[i]));
            }

            return matcherMask;
        }
        #endregion

        #region Private Matcher
        private readonly ulong _mask;

        private Matcher(ulong mask)
        {
            _mask = mask;
        }

        public ulong Mask { get { return _mask; } }

        public bool Match(ulong mask)
        {
            return BitMaskHelper.HasFlagInMask(mask, _mask);
        }
        #endregion
    }
}