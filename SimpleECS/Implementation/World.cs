﻿using System;
using System.Collections.Generic;
using Ecs.Contracts;
using Ecs.Contracts.Entities;
using Ecs.Implementation.Entities;

namespace Ecs.Implementation
{
    public class World : IWorld
    {
        private readonly IEntityManager _entityManager;

        public World(int initialEntityPoolSize, int componentSize)
        {
            if (initialEntityPoolSize < 0)
            {
                throw new ArgumentException("Pool size can't be less 0");
            }

            if (componentSize < 0)
            {
                throw new ArgumentException("Component size can't be less 0");
            }

            _entityManager = new EntityManager(initialEntityPoolSize, componentSize);
        }

        public IEntity CreateEntity()
        {
            return _entityManager.CreateEntity();
        }

        public void RemoveEntity(IEntity entity)
        {
            _entityManager.RemoveEntity(entity);
        }

        public IList<IEntity> GetAllEntities()
        {
            return _entityManager.GetAllEntities();
        }

        public IList<IEntity> GetEntityGroup(IMatcher matcher)
        {
            return _entityManager.GetEntityGroup(matcher);
        }
    }
}