﻿namespace Ecs.Contracts.Entities
{
    public interface IComponent
    {
        IComponentType Type { get; }
    }
}