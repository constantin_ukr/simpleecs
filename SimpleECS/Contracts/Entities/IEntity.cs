﻿using Ecs.Types;

namespace Ecs.Contracts.Entities
{
    public delegate void ComponentChangedDelegate(IEntity entity, IComponentType componentType, OperationType operationType);

    public interface IEntity
    {
        event ComponentChangedDelegate ComponentChanged;
        ulong ComponentsMask { get; }
        bool HasComponent(IComponentType componentType);
        IComponent GetComponent(IComponentType componentType);
        void AddComponent(IComponent component);
        void RemoveComponent(IComponentType componentType);
        void ResetEntity();
    }
}