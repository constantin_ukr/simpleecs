﻿namespace Ecs.Contracts.Entities
{
    public interface IComponentType
    {
        string Name { get; }
        int Index { get; } 
    }
}