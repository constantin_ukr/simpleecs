﻿using System.Collections.Generic;

namespace Ecs.Contracts.Entities
{
    public interface IEntityManager
    {
        IEntity CreateEntity();
        void RemoveEntity(IEntity entity);
        IList<IEntity> GetAllEntities();
        IList<IEntity> GetEntityGroup(IMatcher matcher);
    }
}