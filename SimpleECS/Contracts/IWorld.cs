﻿using Ecs.Contracts.Entities;

namespace Ecs.Contracts
{
    public interface IWorld : IEntityManager
    {
    }
}