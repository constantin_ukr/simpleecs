﻿namespace Ecs.Contracts
{
    public interface IMatcher
    {
        ulong Mask { get; }
        bool Match(ulong mask);
    }
}