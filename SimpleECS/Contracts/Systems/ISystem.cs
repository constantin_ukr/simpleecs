﻿namespace Ecs.Contracts.Systems
{
    public interface ISystem
    {
        IWorld World { get; set; }
    }
}