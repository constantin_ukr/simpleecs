﻿namespace Ecs.Contracts.Systems
{
    public interface ISystemManager
    {
        void Initialize();
        void Update();
        void FixedUpdate();
        void Add(ISystem system);
        void Remove(ISystem system);
    }
}