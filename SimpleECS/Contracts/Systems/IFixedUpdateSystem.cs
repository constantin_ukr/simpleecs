﻿namespace Ecs.Contracts.Systems
{
    public interface IFixedUpdateSystem : ISystem
    {
        void FixedUpdate();
    }
}