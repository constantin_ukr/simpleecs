﻿namespace Ecs.Contracts.Systems
{
    public interface IUpdateSystem : ISystem
    {
        void Update();
    }
}