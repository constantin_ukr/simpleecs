﻿namespace Ecs.Contracts.Pool
{
    public interface IPoolObjectProvider<T>
    {
        T Create();
    }
}