﻿using System.Collections.Generic;

namespace Ecs.Contracts.Pool
{
    public interface IPool<T>
    {
        T Get();
        void Release(T @object);
        IList<T> GetInUsedEntities();
    }
}