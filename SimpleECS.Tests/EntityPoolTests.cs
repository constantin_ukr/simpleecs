﻿using System;
using Ecs.Implementation.Pool;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ecs.Tests.Tests
{
    public class EntityPoolTests
    {
        [TestMethod]
        public static void CreatePoolPositive()
        {
            var pool = new EntityPool(10, new EntityPoolObjectProvider(1));

            var usedEnitas = pool.GetInUsedEntities().Count;

            Assert.AreEqual(usedEnitas, 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public static void CreatePoolWithNegativePoolSize()
        {
            Assert.ThrowsException<ArgumentException>(() => new EntityPool(-1, new EntityPoolObjectProvider(1)));                      
        }

        [TestMethod]
        public static void GetEntity()
        {
            var pool = new EntityPool(10, new EntityPoolObjectProvider(1));

            var entity = pool.Get();
            var usedEntityCount = pool.GetInUsedEntities().Count;
            
            Assert.IsNotNull(entity);
            Assert.AreEqual(usedEntityCount, 1);
        }

        [TestMethod]
        public static void GetEntityFromPoolMoreThanInitialPoolSize()
        {
            var pool = new EntityPool(5, new EntityPoolObjectProvider(1));
            var needEnityCount = 10;

            for (int i = 0; i < needEnityCount; i++)
            {
                pool.Get();
            }            
            var usedEntityCount = pool.GetInUsedEntities().Count;

            Assert.AreEqual(usedEntityCount, needEnityCount);
        }

        [TestMethod]
        public static void GetInUsedEntities()
        {
            var pool = new EntityPool(10, new EntityPoolObjectProvider(1));

            pool.Get();
            var usedEntityCount = pool.GetInUsedEntities().Count;

            Assert.AreEqual(usedEntityCount, 1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public static void ReleaseNullEntity()
        {
            var pool = new EntityPool(10, new EntityPoolObjectProvider(1));

            Assert.ThrowsException<ArgumentNullException>(() => pool.Release(null));
        }

        [TestMethod]
        public static void ReleaseEntity()
        {
            var pool = new EntityPool(10, new EntityPoolObjectProvider(1));

            var entity = pool.Get();            
            var usedEntityCountBeforeRelease = pool.GetInUsedEntities().Count;
            pool.Release(entity);
            var usedEntityCountAfterRelease = pool.GetInUsedEntities().Count;

            Assert.AreEqual(usedEntityCountBeforeRelease, 1);
            Assert.AreEqual(usedEntityCountAfterRelease, 0);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public static void DoubleReleaseEntity()
        {
            var pool = new EntityPool(10, new EntityPoolObjectProvider(1));

            var entity = pool.Get();
            pool.Release(entity);

            Assert.ThrowsException<InvalidOperationException>(() => pool.Release(entity));
        }
    }
}
