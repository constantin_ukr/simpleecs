﻿using Ecs.Contracts.Entities;

namespace Ecs.Tests.Tests.Mocks
{
    public class TestViewComponent : IComponent
    {
        public IComponentType Type { get { return TestComponentType.View; } }
    }
}