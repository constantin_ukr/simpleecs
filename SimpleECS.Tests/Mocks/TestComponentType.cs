using Ecs.Contracts.Entities;

namespace Ecs.Tests.Tests.Mocks
{
    public sealed class TestComponentType : IComponentType
    {
        public static readonly IComponentType Id = new TestComponentType("Id");
        public static readonly IComponentType Position = new TestComponentType("Position");
        public static readonly IComponentType View = new TestComponentType("View");
        public static readonly IComponentType Speed = new TestComponentType("Speed");

        public static int TotalCount; 

        private TestComponentType(string name)
        {
            Name = name;
            Index = TotalCount++;
        }

        public string Name { get; }
        public int Index { get; }
    }
}