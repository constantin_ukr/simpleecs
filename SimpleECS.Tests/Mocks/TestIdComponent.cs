using Ecs.Contracts.Entities;

namespace Ecs.Tests.Tests.Mocks
{
    public class TestIdComponent : IComponent
    {
        public IComponentType Type
        {
            get { return TestComponentType.Id; }
        }
    }
}