﻿using Ecs.Contracts.Entities;

namespace Ecs.Tests.Tests.Mocks
{
    public class TestSpeedComponent : IComponent
    {
        public IComponentType Type { get { return TestComponentType.Speed; } }
    }
}