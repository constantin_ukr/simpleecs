using System.Collections.Generic;
using Ecs.Contracts;
using Ecs.Contracts.Entities;

namespace Ecs.Tests.Tests.Mocks
{
    public class TestWorld : IWorld
    {
        private readonly IList<IEntity> _emptyEntities;

        public TestWorld()
        {
            _emptyEntities = new List<IEntity>(0);
        }

        public IEntity CreateEntity()
        {
            return null;
        }

        public void RemoveEntity(IEntity entity)
        {
        }

        public IList<IEntity> GetAllEntities()
        {
            return _emptyEntities;
        }

        public IList<IEntity> GetEntityGroup(IMatcher matcher)
        {
            return _emptyEntities;
        }
    }
}