using Ecs.Contracts;
using Ecs.Contracts.Systems;

namespace Ecs.Tests.Tests.Mocks
{
    public class TestInitializeSystem : IInitializeSystem
    {
        public bool IsInitialized;

        public IWorld World { get; set; }

        public void Initialize()
        {
            IsInitialized = true;
        }
    }
}