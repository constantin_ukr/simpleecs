﻿using Ecs.Contracts.Entities;

namespace Ecs.Tests.Tests.Mocks
{
    public class TestPositionComponent : IComponent
    {
        public IComponentType Type
        {
            get { return TestComponentType.Position; }
        }
    }
}