﻿using Ecs.Contracts;
using Ecs.Contracts.Systems;

namespace Ecs.Tests.Tests.Mocks
{
    public class TestComplexSystem : IInitializeSystem, IUpdateSystem
    {
        public bool IsInitialized;
        public bool IsUpdated;

        public IWorld World { get; set; }

        public void Initialize()
        {
            IsInitialized = true;
        }

        public void Update()
        {
            IsUpdated = true;
        }
    }
}