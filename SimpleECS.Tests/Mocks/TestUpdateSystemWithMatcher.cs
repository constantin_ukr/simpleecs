﻿using Ecs.Contracts;
using Ecs.Contracts.Entities;
using Ecs.Contracts.Systems;
using Ecs.Implementation;

namespace Ecs.Tests.Tests.Mocks
{
    public class TestUpdateSystemWithMatcher : IUpdateSystem
    {
        public bool IsEntityGot;

        private readonly IEntity _entity;
        private readonly IMatcher _matcher;

        public IWorld World { get; set; }

        public TestUpdateSystemWithMatcher(IEntity entity)
        {
            _entity = entity;
            _matcher = Matcher.GetMatcher(TestComponentType.View, TestComponentType.Position);
        }

        public void Update()
        {
            var items = World.GetEntityGroup(_matcher);
            IsEntityGot = items.Contains(_entity);

            if (IsEntityGot && items[0].HasComponent(TestComponentType.View))
            {
                items[0].RemoveComponent(TestComponentType.View);
            }
        }
    }
}