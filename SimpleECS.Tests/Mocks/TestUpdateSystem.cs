using Ecs.Contracts;
using Ecs.Contracts.Systems;

namespace Ecs.Tests.Tests.Mocks
{
    public class TestUpdateSystem : IUpdateSystem
    {
        public bool IsUpdated;

        public IWorld World { get; set; }

        public void Update()
        {
            IsUpdated = true;
        }
    }
}