using Ecs.Contracts;
using Ecs.Contracts.Systems;

namespace Ecs.Tests.Tests.Mocks
{
    public class TestFixedUpdateSystem : IFixedUpdateSystem
    {
        public bool IsFixedUpdated;

        public IWorld World { get; set; }

        public void FixedUpdate()
        {
            IsFixedUpdated = true;
        }
    }
}