﻿using Ecs.Contracts;
using Ecs.Contracts.Systems;

namespace Ecs.Tests.Tests.Mocks
{
    public class TestAllComplexSystem : IInitializeSystem, IUpdateSystem, IFixedUpdateSystem
    {
        public bool IsInitialized;
        public bool IsUpdated;
        public bool IsFixedUpdated;

        public IWorld World { get; set; }

        public void Initialize()
        {
            IsInitialized = true;
        }

        public void Update()
        {
            IsUpdated = true;
        }

        public void FixedUpdate()
        {
            IsFixedUpdated = true;
        }
    }
}