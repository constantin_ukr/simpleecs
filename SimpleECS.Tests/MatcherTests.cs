﻿using System;
using Ecs.Contracts.Entities;
using Ecs.Helpers;
using Ecs.Implementation;
using Ecs.Tests.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ecs.Tests
{
    public static class MatcherTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public static void CreateMatcherWithNullParameters()
        {
            Assert.ThrowsException<ArgumentException>(() => Matcher.GetMatcher());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public static void CreateEmptyMatcher()
        {
            Assert.ThrowsException<ArgumentException>(() => Matcher.GetMatcher(new IComponentType[] {}));
        }

        [TestMethod]
        public static void CreateSameMatcher()
        {
            var testMatcher1 = Matcher.GetMatcher(TestComponentType.View, TestComponentType.Position);
            var testMatcher2 = Matcher.GetMatcher(TestComponentType.View, TestComponentType.Position);

            Assert.AreEqual(testMatcher1, testMatcher2);
        }

        [TestMethod]
        public static void NotMatch()
        {
            ulong entityMask = BitMaskHelper.ToBitMask(TestComponentType.Position) | BitMaskHelper.ToBitMask(TestComponentType.Id);
            var test = Matcher.GetMatcher(TestComponentType.View, TestComponentType.Position);

            Assert.IsFalse(test.Match(entityMask));
        }

        [TestMethod]
        public static void MatchInMany()
        {
            ulong entityMask = BitMaskHelper.ToBitMask(TestComponentType.Position) | BitMaskHelper.ToBitMask(TestComponentType.Id) | BitMaskHelper.ToBitMask(TestComponentType.View);
            var test = Matcher.GetMatcher(TestComponentType.View, TestComponentType.Position);

            Assert.IsTrue(test.Match(entityMask));
        }

        [TestMethod]
        public static void MatchInSame()
        {
            ulong entityMask = BitMaskHelper.ToBitMask(TestComponentType.Position) | BitMaskHelper.ToBitMask(TestComponentType.View);
            var test = Matcher.GetMatcher(TestComponentType.View, TestComponentType.Position);

            Assert.IsTrue(test.Match(entityMask));
        }
    }
}