﻿using Ecs.Helpers;
using Ecs.Tests.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Ecs.Tests
{
    public static class HelpersTests
    {
        [TestMethod]
        public static void EnumToBitMask()
        {
            ulong rotationMask = BitMaskHelper.ToBitMask(TestComponentType.View);

            Assert.AreEqual(rotationMask, 4);
        }

        [TestMethod]
        public static void AddFlagToMask()
        {
            ulong rotationMask = BitMaskHelper.ToBitMask(TestComponentType.Id);
            BitMaskHelper.AddFlagToMask(ref rotationMask, BitMaskHelper.ToBitMask(TestComponentType.View));

            Assert.AreEqual(rotationMask, 5);
        }

        [TestMethod]
        public static void RemoveFlagFromMask()
        {
            ulong rotationMask = BitMaskHelper.ToBitMask(TestComponentType.View) | BitMaskHelper.ToBitMask(TestComponentType.Id);
            BitMaskHelper.RemoveFlagFromMask(ref rotationMask, BitMaskHelper.ToBitMask(TestComponentType.View));

            Assert.AreEqual(rotationMask, 1);
        }

        [TestMethod]
        public static void HasFlagInMask()
        {
            ulong rotationMask = BitMaskHelper.ToBitMask(TestComponentType.View) | BitMaskHelper.ToBitMask(TestComponentType.Id);

            Assert.IsTrue(BitMaskHelper.HasFlagInMask(rotationMask, BitMaskHelper.ToBitMask(TestComponentType.View)));
        }

        [TestMethod]
        public static void ManyToBitMask()
        {
            ulong rotationMask = BitMaskHelper.ManyToBitMask(TestComponentType.View, TestComponentType.Id);

            Assert.AreEqual(rotationMask, 5);
        }

        [TestMethod]
        public static void EmptyArrayToBitMask()
        {
            Exception exception = null;
            try
            {
                BitMaskHelper.ManyToBitMask();
            }
            catch(Exception ex)
            {
                exception = ex;
            }
            Assert.IsNotNull(exception);
        }
    }
}