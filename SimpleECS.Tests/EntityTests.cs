﻿using System;
using Ecs.Contracts.Entities;
using Ecs.Implementation.Entities;
using Ecs.Tests.Tests.Mocks;
using Ecs.Types;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ecs.Tests
{
    public static class EntityTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public static void CreateEntityWithNegativeComponentSize()
        {
            Assert.ThrowsException<ArgumentException>(() => new Entity(-1));
        }

        [TestMethod]
        public static void EntityAddComponent()
        {
            var entity = new Entity(10);
            entity.AddComponent(new TestIdComponent());

            Assert.IsTrue(entity.HasComponent(TestComponentType.Id));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public static void EntityDoubleAddComponent()
        {
            var entity = new Entity(10);
            entity.AddComponent(new TestIdComponent());

            Assert.ThrowsException<InvalidOperationException>(() => entity.AddComponent(new TestIdComponent()));
        }

        [TestMethod]
        public static void EntityNotHasComponent()
        {
            var entity = new Entity(10);

            Assert.IsFalse(entity.HasComponent(TestComponentType.Id));
        }

        [TestMethod]
        public static void EntityRemoveComponent()
        {
            var entity = new Entity(10);
            entity.AddComponent(new TestIdComponent());
            entity.RemoveComponent(TestComponentType.Id);

            Assert.IsFalse(entity.HasComponent(TestComponentType.Id));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public static void EntityDoubleRemoveComponent()
        {
            var entity = new Entity(10);
            entity.AddComponent(new TestIdComponent());
            entity.RemoveComponent(TestComponentType.Id);

            Assert.ThrowsException<InvalidOperationException>(() => entity.RemoveComponent(TestComponentType.Id));
        }

        [TestMethod]
        public static void EntityGetComponent()
        {
            var entity = new Entity(10);
            var component = new TestIdComponent();
            entity.AddComponent(component);
            var getComponent = entity.GetComponent(TestComponentType.Id);

            Assert.AreEqual(component, getComponent);
        }

        [TestMethod]
        public static void EntityGetNullComponent()
        {
            var entity = new Entity(10);
            var getComponent = entity.GetComponent(TestComponentType.Id);

            Assert.IsNull(getComponent);
        }

        [TestMethod]
        public static void EntityResetComponents()
        {
            var entity = new Entity(100);
            entity.AddComponent(new TestPositionComponent());
            entity.AddComponent(new TestIdComponent());
            entity.ResetEntity();

            var result = !entity.HasComponent(TestComponentType.Position)
                && !entity.HasComponent(TestComponentType.Id)
                && entity.GetComponent(TestComponentType.Position) == null
                && entity.GetComponent(TestComponentType.Id) == null;
                
            Assert.IsTrue(result);
        }

        [TestMethod]
        public static void EntityAddSendEventComponent()
        {
            var entity = new Entity(10);

            var result = false;

            ComponentChangedDelegate action = null;
            action = (entityParam, componentType, operationType) =>
            {
                if (operationType == OperationType.Add && TestComponentType.Position == componentType)
                {
                    result = true;
                }
            };

            entity.ComponentChanged += action;
            entity.AddComponent(new TestPositionComponent());
            entity.ComponentChanged -= action;

            Assert.IsTrue(result);
        }

        [TestMethod]
        public static void EntityRemoveSendEventComponent()
        {
            var entity = new Entity(10);

            var result = false;
            entity.AddComponent(new TestPositionComponent());

            ComponentChangedDelegate action = null;
            action = (entityParam, componentType, operationType) =>
            {
                if (operationType == OperationType.Remove && TestComponentType.Position == componentType)
                {
                    result = true;
                }
            };

            entity.ComponentChanged += action;
            entity.RemoveComponent(TestComponentType.Position);
            entity.ComponentChanged -= action;

            Assert.IsTrue(result);
        }

        [TestMethod]
        public static void EntityResetSendEventComponent()
        {
            var entity = new Entity(10);

            int removeComponentCount = 0;
 
            entity.AddComponent(new TestPositionComponent());
            entity.AddComponent(new TestIdComponent());

            ComponentChangedDelegate action = null;
            action = (entityParam, componentType, operationType) =>
            {
                if (operationType == OperationType.Remove && TestComponentType.Position == componentType)
                {
                    removeComponentCount++;
                }
                if (operationType == OperationType.Remove && TestComponentType.Id == componentType)
                {
                    removeComponentCount++;
                }
            };

            entity.ComponentChanged += action;

            entity.RemoveComponent(TestComponentType.Position);
            entity.RemoveComponent(TestComponentType.Id);
            entity.ComponentChanged -= action;

            Assert.IsTrue(removeComponentCount == 2);
        }
    }
}