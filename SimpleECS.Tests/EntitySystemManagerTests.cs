﻿using System;
using Ecs.Implementation;
using Ecs.Implementation.Systems;
using Ecs.Tests.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ecs.Tests
{
    public static class EntitySystemManagerTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public static void CreateSystemManagerWithNullParameter()
        {
            Assert.ThrowsException<ArgumentException>(() => new EntitySystemManager(null));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public static void AddNullSystem()
        {
            var world = new TestWorld();
            var systemManager = new EntitySystemManager(world);

            Assert.ThrowsException<ArgumentException>(() => systemManager.Add(null));
        }

        [TestMethod]
        public static void AddUpdateSystem()
        {
            var world = new TestWorld();
            var systemManager = new EntitySystemManager(world);
            var system = new TestUpdateSystem();

            systemManager.Add(system);
            systemManager.Update();

            Assert.IsTrue(system.IsUpdated);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public static void DoubleAddUpdateSystem()
        {
            var world = new TestWorld();
            var systemManager = new EntitySystemManager(world);
            var system = new TestUpdateSystem();

            systemManager.Add(system);

            Assert.ThrowsException<InvalidOperationException>(() => systemManager.Add(system));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public static void RemoveNullSystem()
        {
            var world = new TestWorld();
            var systemManager = new EntitySystemManager(world);

            Assert.ThrowsException<ArgumentException>(() => systemManager.Remove(null));
        }

        [TestMethod]
        public static void RemoveUpdateSystem()
        {
            var world = new TestWorld();
            var systemManager = new EntitySystemManager(world);
            var system = new TestUpdateSystem();

            systemManager.Add(system);
            systemManager.Remove(system);
            systemManager.Update();

            Assert.IsFalse(system.IsUpdated);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public static void DoubleRemoveUpdateSystem()
        {
            var world = new TestWorld();
            var systemManager = new EntitySystemManager(world);
            var system = new TestUpdateSystem();

            systemManager.Add(system);
            systemManager.Remove(system);

            Assert.ThrowsException<InvalidOperationException>(() => systemManager.Remove(system));
        }

        [TestMethod]
        public static void AddInitializeSystem()
        {
            var world = new TestWorld();
            var systemManager = new EntitySystemManager(world);
            var system = new TestInitializeSystem();

            systemManager.Add(system);
            systemManager.Initialize();

            Assert.IsTrue(system.IsInitialized);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public static void DoubleAddInitializeSystem()
        {
            var world = new TestWorld();
            var systemManager = new EntitySystemManager(world);
            var system = new TestInitializeSystem();

            systemManager.Add(system);

            Assert.ThrowsException<InvalidOperationException>(() => systemManager.Add(system));
        }

        [TestMethod]
        public static void RemoveInitializeSystem()
        {
            var world = new TestWorld();
            var systemManager = new EntitySystemManager(world);
            var system = new TestInitializeSystem();

            systemManager.Add(system);
            systemManager.Remove(system);
            systemManager.Initialize();

            Assert.IsFalse(system.IsInitialized);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public static void DoubleRemoveInitializeSystem()
        {
            var world = new TestWorld();
            var systemManager = new EntitySystemManager(world);
            var system = new TestInitializeSystem();

            systemManager.Add(system);
            systemManager.Remove(system);

            Assert.ThrowsException<InvalidOperationException>(() => systemManager.Remove(system));
        }

        [TestMethod]
        public static void AddFixedUpdateSystem()
        {
            var world = new TestWorld();
            var systemManager = new EntitySystemManager(world);
            var system = new TestFixedUpdateSystem();

            systemManager.Add(system);
            systemManager.FixedUpdate();

            Assert.IsTrue(system.IsFixedUpdated);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public static void DoubleAddFixedUpdateSystem()
        {
            var world = new TestWorld();
            var systemManager = new EntitySystemManager(world);
            var system = new TestFixedUpdateSystem();

            systemManager.Add(system);

            Assert.ThrowsException<InvalidOperationException>(() => systemManager.Add(system));
        }

        [TestMethod]
        public static void RemoveFixedUpdateSystem()
        {
            var world = new TestWorld();
            var systemManager = new EntitySystemManager(world);
            var system = new TestFixedUpdateSystem();

            systemManager.Add(system);
            systemManager.Remove(system);
            systemManager.FixedUpdate();

            Assert.IsFalse(system.IsFixedUpdated);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public static void DoubleRemoveFixedUpdateSystem()
        {
            var world = new TestWorld();
            var systemManager = new EntitySystemManager(world);
            var system = new TestFixedUpdateSystem();

            systemManager.Add(system);
            systemManager.Remove(system);

            Assert.ThrowsException<InvalidOperationException>(() => systemManager.Remove(system));
        }

        [TestMethod]
        public static void AddComplexSystem()
        {
            var world = new TestWorld();
            var systemManager = new EntitySystemManager(world);
            var system = new TestComplexSystem();

            systemManager.Add(system);
            systemManager.Initialize();
            systemManager.Update();

            Assert.IsTrue(system.IsInitialized);
            Assert.IsTrue(system.IsUpdated);
        }

        [TestMethod]
        public static void RemoveComplexSystem()
        {
            var world = new TestWorld();
            var systemManager = new EntitySystemManager(world);
            var system = new TestComplexSystem();

            systemManager.Add(system);
            systemManager.Remove(system);
            systemManager.Initialize();
            systemManager.Update();

            Assert.IsFalse(system.IsInitialized);
            Assert.IsFalse(system.IsUpdated);
        }

        [TestMethod]
        public static void AddAllComplexSystem()
        {
            var world = new TestWorld();
            var systemManager = new EntitySystemManager(world);
            var system = new TestAllComplexSystem();

            systemManager.Add(system);
            systemManager.Initialize();
            systemManager.Update();
            systemManager.FixedUpdate();

            Assert.IsTrue(system.IsInitialized);
            Assert.IsTrue(system.IsUpdated);
            Assert.IsTrue(system.IsFixedUpdated);
        }

        [TestMethod]
        public static void RemoveAllComplexSystem()
        {
            var world = new TestWorld();
            var systemManager = new EntitySystemManager(world);
            var system = new TestAllComplexSystem();

            systemManager.Add(system);
            systemManager.Remove(system);
            systemManager.Initialize();
            systemManager.Update();
            systemManager.FixedUpdate();

            Assert.IsFalse(system.IsInitialized);
            Assert.IsFalse(system.IsUpdated);
            Assert.IsFalse(system.IsFixedUpdated);
        }

        [TestMethod]
        public static void GetEntitiesOnUpdateSystem()
        {
            var world = new World(100, 5);
            var systemManager = new EntitySystemManager(world);
            var entity = world.CreateEntity();
            var system = new TestUpdateSystemWithMatcher(entity);

            entity.AddComponent(new TestViewComponent());
            entity.AddComponent(new TestPositionComponent());
            systemManager.Add(system);
            systemManager.Initialize();
            systemManager.Update();
            var isEntityGotBeforeRemove = system.IsEntityGot;
            systemManager.Update();
            var isEntityGotAfterRemove = system.IsEntityGot;

            Assert.IsTrue(isEntityGotBeforeRemove);
            Assert.IsFalse(isEntityGotAfterRemove);
        }
    }
}