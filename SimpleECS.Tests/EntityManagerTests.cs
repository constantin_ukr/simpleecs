﻿using System;
using Ecs.Implementation;
using Ecs.Implementation.Entities;
using Ecs.Tests.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ecs.Tests.Tests
{
    public class EntityManagerTests
    {
        [TestMethod]
        public static void GetAllEntities()
        {
            var entityManger = new EntityManager(10, 10);
            var usedEntityCount = entityManger.GetAllEntities().Count;

            Assert.IsTrue(usedEntityCount == 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public static void CreateEntityManagerWithNegativePoolSize()
        {
            Assert.ThrowsException<ArgumentException>(() => new EntityManager(-1, 10));
        }

        [TestMethod]
        public static void CreateEntity()
        {
            var entityManger = new EntityManager(10, 10);

            entityManger.CreateEntity();
            var usedEntityCount = entityManger.GetAllEntities().Count;

            Assert.IsTrue(usedEntityCount == 1);
        }

        [TestMethod]
        public static void RemoveEntity()
        {
            var entityManger = new EntityManager(10, 10);

            var entity = entityManger.CreateEntity();
            var usedEntityBeforeCount = entityManger.GetAllEntities().Count;
            entityManger.RemoveEntity(entity);
            var usedEntityAfterCount = entityManger.GetAllEntities().Count;

            Assert.IsTrue(usedEntityBeforeCount == 1 && usedEntityAfterCount == 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public static void RemoveNullEntity()
        {
            var entityManger = new EntityManager(10, 10);

            Assert.ThrowsException<ArgumentException>(() => entityManger.RemoveEntity(null));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public static void DoubleRemoveEntity()
        {
            var entityManger = new EntityManager(10, 10);
            var entityOne = entityManger.CreateEntity();

            entityManger.RemoveEntity(entityOne);

            Assert.ThrowsException<InvalidOperationException>(() => entityManger.RemoveEntity(entityOne));
        }

        [TestMethod]
        public static void AddEntityToGroup()
        {
            var entityManger = new EntityManager(10, 10);
            var testMatcher = Matcher.GetMatcher(TestComponentType.Id, TestComponentType.View, TestComponentType.Position);

            var entity = entityManger.CreateEntity();
            entity.AddComponent(new TestIdComponent());
            entity.AddComponent(new TestViewComponent());

            var isExistedInGroupBeforeAdd = entityManger.GetEntityGroup(testMatcher).Contains(entity);
            entity.AddComponent(new TestPositionComponent());
            var isNotExistedInGroupAfterAdd = entityManger.GetEntityGroup(testMatcher).Contains(entity);

            Assert.IsFalse(isExistedInGroupBeforeAdd);
            Assert.IsTrue(isNotExistedInGroupAfterAdd);
        }

        [TestMethod]
        public static void RemoveEntityFromGroup()
        {
            var entityManger = new EntityManager(10, 10);
            var testMatcher = Matcher.GetMatcher(TestComponentType.Id, TestComponentType.Position);

            var entity = entityManger.CreateEntity();
            entity.AddComponent(new TestIdComponent());
            entity.AddComponent(new TestPositionComponent());
            entity.AddComponent(new TestViewComponent());

            var isExistedInGroupBeforeRemove = entityManger.GetEntityGroup(testMatcher).Contains(entity);
            entity.RemoveComponent(TestComponentType.Position);
            var isNotExistedInGroupAfterRemove = entityManger.GetEntityGroup(testMatcher).Contains(entity);

            Assert.IsTrue(isExistedInGroupBeforeRemove);
            Assert.IsFalse(isNotExistedInGroupAfterRemove);
        }

        [TestMethod]
        public static void ExistEntityInBothGroups()
        {
            var entityManger = new EntityManager(10, 10);
            var testMatcher1 = Matcher.GetMatcher(TestComponentType.Id, TestComponentType.Position);
            var testMatcher2 = Matcher.GetMatcher(TestComponentType.Id, TestComponentType.Position, TestComponentType.View);

            var entity = entityManger.CreateEntity();
            entity.AddComponent(new TestIdComponent());
            entity.AddComponent(new TestPositionComponent());
            entity.AddComponent(new TestViewComponent());

            var items1 = entityManger.GetEntityGroup(testMatcher1);
            var items2 = entityManger.GetEntityGroup(testMatcher2);

            Assert.IsTrue(items1.Count > 0);
            Assert.IsTrue(items2.Count > 0);
            Assert.IsTrue(items1.Count == items2.Count);
            Assert.AreEqual(items1[0], items2[0]);
        }

        [TestMethod]
        public static void ExistEntityInShrinkedGroup()
        {
            var entityManger = new EntityManager(10, 10);
            var testMatcher1 = Matcher.GetMatcher(TestComponentType.Id, TestComponentType.Position);
            var testMatcher2 = Matcher.GetMatcher(TestComponentType.Id, TestComponentType.Position, TestComponentType.View);

            var entity = entityManger.CreateEntity();
            entity.AddComponent(new TestIdComponent());
            entity.AddComponent(new TestPositionComponent());
            entity.AddComponent(new TestSpeedComponent());

            var items2 = entityManger.GetEntityGroup(testMatcher2);
            var items1 = entityManger.GetEntityGroup(testMatcher1);

            Assert.IsTrue(items2.Count == 0);
            Assert.IsTrue(items1.Count > 0);
            Assert.AreEqual(items1[0], entity);
        }

        [TestMethod]
        public static void ExistEntityInGroupBeforeCreateEntity()
        {
            var entityManger = new EntityManager(10, 10);
            var testMatcher1 = Matcher.GetMatcher(TestComponentType.Id, TestComponentType.Position);
            var testMatcher2 = Matcher.GetMatcher(TestComponentType.Id, TestComponentType.Position, TestComponentType.View);

            var items1 = entityManger.GetEntityGroup(testMatcher1);
            var items2 = entityManger.GetEntityGroup(testMatcher2);

            var entity = entityManger.CreateEntity();
            entity.AddComponent(new TestIdComponent());
            entity.AddComponent(new TestPositionComponent());
            entity.AddComponent(new TestViewComponent());

            Assert.IsTrue(items1.Count > 0);
            Assert.IsTrue(items2.Count > 0);
            Assert.AreEqual(items1[0], entity);
            Assert.AreEqual(items2[0], entity);
        }
    }
}
