﻿using System;
using Ecs.Implementation.Pool;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ecs.Tests.Tests
{
    public class EntityPoolObjectProviderTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public static void CreatePoolObjectProviderWithNegativeComponentSize()
        {
            Assert.ThrowsException<ArgumentException>(() => new EntityPoolObjectProvider(-1));
        }

        [TestMethod]
        public static void CreateEntityViaEntityPoolObjectProvider()
        {
            var entityPoolObjectProvider = new EntityPoolObjectProvider(10);

            var entity = entityPoolObjectProvider.Create();

            Assert.IsNotNull(entity);
        }
    }
}